<?php
    //include table creation file
    include_once('table_creation.php');
?>

<!-- Start Script to import the data from CSV to MySQL database -->
<?php
    $query = "SELECT passenger_id FROM passenger";
    $table_row_count = $dbc->query($query)->num_rows;
    
    if($table_row_count == 0) {
        $row = 1;
        if (($handle = fopen(__DIR__."/titanic.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $result_array = array();
                $num = count($data);
                if($row == 1) {
                    $row++;
                    continue;
                }
                else {
                    for ($c=0; $c < $num; $c++) {
                        $result_array[] = '"'.$data[$c].'"';
                    }
                
                    $input_passenger = implode(",", $result_array);
                    $passenger_input_query = "INSERT INTO passenger(`survived`,`pclass`,`name`,`sex`,`age`,`siblings_abroad`,`parents_abroad`,`fare`) VALUE($input_passenger)";
                    $dbc->query($passenger_input_query);
                }

                $row++;
            }

            fclose($handle);
        }
    }
    
?>
<!-- End Script to import the data from CSV to MySQL database -->