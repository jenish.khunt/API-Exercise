<?php
    // Include database connection file if not exist
    include_once('db_connection.php');
    
    // Table creation
    mysqli_query($dbc, "
        CREATE TABLE IF NOT EXISTS `passenger` ( 
            `passenger_id` INT(11) NOT NULL AUTO_INCREMENT , 
            `name` TEXT NOT NULL , 
            `sex` VARCHAR(6) NOT NULL , 
            `age` INT(3) NOT NULL , 
            `siblings_abroad` TINYINT(1) NOT NULL , 
            `parents_abroad` TINYINT(1) NOT NULL , 
            `fare` FLOAT(11) NOT NULL , `survived` TINYINT(1) NOT NULL , 
            `pclass` TINYINT(1) NOT NULL , 
            PRIMARY KEY (`passenger_id`)
        )
    ");
?>