<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
 
// instantiate people object
include_once '../objects/people.php';
 
$database = new Database();
$db = $database->getConnection();
 
$people = new People($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));  
 
// make sure data is not empty
if(
    !empty($data->name) &&
    !empty($data->sex) &&
    !empty($data->age) &&
    !empty($data->fare)
){
 
    // set people property values
    $people->name = $data->name;
    $people->sex = $data->sex;
    $people->age = $data->age;
    $people->siblings_abroad = $data->siblingsOrSpousesAboard;
    $people->parents_abroad = $data->parentsOrChildrenAboard;
    $people->fare = $data->fare;
    $people->survived = $data->survived ? 1 : 0;
    $people->pclass = $data->passengerClass;
    $people->created = date('Y-m-d H:i:s');
 
    // create the people
    if($people->create()){
 
        $people_arr = array(
            "uuid" => $people->passenger_id,
            "name" => $people->name,
            "sex" => $people->sex,
            "age" => $people->age,
            "siblingsOrSpousesAboard" => $people->siblings_abroad,
            "parentsOrChildrenAboard" => $people->parents_abroad,
            "fare" => $people->fare,
            "survived" => ($people->survived == 1 ) ? true : false,
            "passengerClass" => $people->pclass
        );
        
        // set response code - 201 created
        http_response_code(201);
 
        // tell the user
        echo json_encode($people_arr);
    }
 
    // if unable to create the people, tell the user
    else{
 
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the user
        echo json_encode(array("message" => "Unable to create people."));
    }
}
 
// tell the user data is incomplete
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Unable to create people. Data is incomplete."));
}
?>