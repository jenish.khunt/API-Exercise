<?php
// include database and object files
include_once '../config/database.php';
include_once '../objects/people.php';
 
// instantiate database and people object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$people = new People($db);
 
// query people
$stmt = $people->read();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // people array
    $people_arr=array();
 
    // retrieve our table contents
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // just $name only
        extract($row);
 
        $people_item=array(
            "uuid" => $passenger_id,
            "name" => $name,
            "sex" => $sex,
            "age" => $age,
            "siblingsOrSpousesAboard" => $siblings_abroad,
            "parentsOrChildrenAboard" => $parents_abroad,
            "fare" => $fare,
            "survived" => ($survived == 1 ) ? true : false,
            "passengerClass" => $pclass,
        );
 
        array_push($people_arr, $people_item);
    }
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show people data in json format
    echo json_encode($people_arr);
}
else {
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no people found
    echo json_encode(
        array("message" => "No people found.")
    );
}