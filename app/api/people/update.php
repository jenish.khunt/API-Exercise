<?php
include_once('request.php');

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/people.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare people object
$people = new People($db);
 
// get id of ] to be edited
$data = json_decode(file_get_contents("php://input"));
 
// set ID property of ] to be edited
$people->passenger_id = $_GET['uuid'];
 
// set people property values
$people->name = $data->name;
$people->sex = $data->sex;
$people->age = $data->age;
$people->siblings_abroad = $data->siblingsOrSpousesAboard;
$people->parents_abroad = $data->parentsOrChildrenAboard;
$people->fare = $data->fare;
$people->survived = $data->survived;
$people->passengerClass = $data->passengerClass;
 
// update the people
if($people->update()){
 
    // set response code - 200 ok
    http_response_code(200);
 
    // tell the user
    echo json_encode(array("message" => "People was updated."));
}
 
// if unable to update the people, tell the user
else{
 
    // set response code - 503 service unavailable
    http_response_code(503);
 
    // tell the user
    echo json_encode(array("message" => "Unable to update people."));
}
?>