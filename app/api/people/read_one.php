<?php
include_once('request.php');

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/people.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare people object
$people = new People($db);
 
// set ID property of record to read
$people->passenger_id = isset($_GET['uuid']) ? $_GET['uuid'] : die();
 
// read the details of people to be edited
$people->readOne();
 
if($people->name!=null){
    // create array
    $people_arr = array(
        "uuid" => $people->passenger_id,
        "name" => $people->name,
        "sex" => $people->sex,
        "age" => $people->age,
        "siblingsOrSpousesAboard" => $people->siblings_abroad,
        "parentsOrChildrenAboard" => $people->parents_abroad,
        "fare" => $people->fare,
        "survived" => ($people->survived == 1 ) ? true : false,
        "passengerClass" => $people->pclass
    );
 
    // set response code - 200 OK
    http_response_code(200);
 
    // make it json format
    echo json_encode($people_arr);
}
 
else{
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user people does not exist
    echo json_encode(array("message" => "People does not exist."));
}
?>