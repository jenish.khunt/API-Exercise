<?php
class People{
 
    // database connection and table name
    private $conn;
    private $table_name = "passenger";
 
    // object properties
    public $passenger_id;
    public $name;
    public $sex;
    public $age;
    public $siblings_abroad;
    public $parents_abroad;
    public $fare;
    public $survived;
    public $pclass;
 
    // constructor with $db as database connection
    public function __construct($db) 
    {
        $this->conn = $db;
    }

    // read peoples
    function read() 
    {
    
        // select all query
        $query = "SELECT * FROM " . $this->table_name;
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }

    // create people
    function create(){
    
        // query to insert record
        $query = "INSERT INTO " . $this->table_name . 
                " SET 
                    name=:name, 
                    sex=:sex, 
                    age=:age, 
                    siblings_abroad=:siblings_abroad, 
                    parents_abroad=:parents_abroad, 
                    fare=:fare, survived=:survived, 
                    pclass=:pclass";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->sex=htmlspecialchars(strip_tags($this->sex));
        $this->age=htmlspecialchars(strip_tags($this->age));
        $this->siblings_abroad=htmlspecialchars(strip_tags($this->siblings_abroad));
        $this->parents_abroad=htmlspecialchars(strip_tags($this->parents_abroad));
        $this->fare=htmlspecialchars(strip_tags($this->fare));
        $this->survived=htmlspecialchars(strip_tags($this->survived));
        $this->pclass=htmlspecialchars(strip_tags($this->pclass));
        
    
        // bind values
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":sex", $this->sex);
        $stmt->bindParam(":age", $this->age);
        $stmt->bindParam(":siblings_abroad", $this->siblings_abroad);
        $stmt->bindParam(":parents_abroad", $this->parents_abroad);
        $stmt->bindParam(":fare", $this->fare);
        $stmt->bindParam(":survived", $this->survived);
        $stmt->bindParam(":pclass", $this->pclass);
    
        // execute query
        if($stmt->execute()){
            $this->passenger_id = $this->conn->lastInsertId();
            return true;
        }

        return false;
    }

    // used when filling up the update people form
    function readOne(){
    
        // query to read single record
        $query = "SELECT * FROM " . $this->table_name . " WHERE passenger_id= :passenger_id";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->passenger_id=htmlspecialchars(strip_tags($this->passenger_id));

        // bind values
        $stmt->bindParam(":passenger_id", $this->passenger_id);
    
        // execute query
        $stmt->execute();
    
        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
    
        // set values to object properties
        $this->passenger_id = $row['passenger_id'];
        $this->name = $row['name'];
        $this->sex = $row['sex'];
        $this->age = $row['age'];
        $this->siblings_abroad = $row['siblings_abroad'];
        $this->parents_abroad = $row['parents_abroad'];
        $this->fare = $row['fare'];
        $this->survived = $row['survived'];
        $this->pclass = $row['pclass'];
    }

    // update the people
    function update(){
    
        // update query
        $query = "UPDATE
                    " . $this->table_name . "
                SET 
                    name=:name, 
                    sex=:sex, 
                    age=:age, 
                    siblings_abroad=:siblings_abroad, 
                    parents_abroad=:parents_abroad, 
                    fare=:fare, survived=:survived, 
                    pclass=:pclass
                WHERE
                    passenger_id = :passenger_id";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->sex=htmlspecialchars(strip_tags($this->sex));
        $this->age=htmlspecialchars(strip_tags($this->age));
        $this->siblings_abroad=htmlspecialchars(strip_tags($this->siblings_abroad));
        $this->parents_abroad=htmlspecialchars(strip_tags($this->parents_abroad));
        $this->fare=htmlspecialchars(strip_tags($this->fare));
        $this->survived=htmlspecialchars(strip_tags($this->survived));
        $this->pclass=htmlspecialchars(strip_tags($this->pclass));
        $this->passenger_id=htmlspecialchars(strip_tags($this->passenger_id));
    
        // bind values
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":sex", $this->sex);
        $stmt->bindParam(":age", $this->age);
        $stmt->bindParam(":siblings_abroad", $this->siblings_abroad);
        $stmt->bindParam(":parents_abroad", $this->parents_abroad);
        $stmt->bindParam(":fare", $this->fare);
        $stmt->bindParam(":survived", $this->survived);
        $stmt->bindParam(":pclass", $this->pclass);
        $stmt->bindParam(":passenger_id", $this->passenger_id);
        
        // execute the query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }

    // delete the people
    function delete(){
    
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE passenger_id = ?";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->passenger_id=htmlspecialchars(strip_tags($this->passenger_id));
    
        // bind id of record to delete
        $stmt->bindParam(1, $this->passenger_id);
    
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
        
    }
}