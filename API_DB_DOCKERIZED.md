# API-exercise

Thanks for giving the opportunity performing this exercise.

### 0. Fork this repository
Created a fork and commited (Cool idea, I never did this before).

### 1. Setup & fill database

I have been most familiar with the MySQL database. The given requirements can be achieved using SQL / NoSQL and I opted for the SQL one.

<b> Database and Table creation </b> 
- I had to think of the schema and optimization of it as we speak about the SQL now. 
- I have created a schema for a Passenger table which gets the primary key and other fields which have optimized data type (Schema present under database_import/table_creation.php).

- Challenge 1 -> The most important and challenging was to make this import process automated. I have worked on PHP in the past and I have more hands-on converting CSV to different data format using it and I chose to go with PHP.

- Challenge 2 -> was to structure the scripts for import process.

- Folder Structure
    ```
        database_import
		- /db_connection.php
		- /table_creation.php
		- /db_creation.php```

- <b>Db_connection.php</b> -> It connects to the MySQL server. AND CREATES THE TABLE `api_exercise` it doesn’t exist which automates the database creation process.

- <b>Table_creation.php</b> -> After the DATABASE, This creates the table `passenger` it doesn’t exist.

- <b> db_creation.php</b> - It converts the CSV data to MYSQL.

### 2. Create an API

- As I have chosen for the PHP, I wanted to implement the APIs using object-oriented and with the proper data structure.

- Challenge 1 -> was to keep it well organized in terms of file structure and tried to make it loosely coupled.

- Folder Structure
		```
		
          /api
			- /config
			  - Database connections
			- /objects
		        - CRUD Objects
			- /people
			  - Different API Functions.```


- This covers all the APIs exactly the way it has been asked for. I finished up the coding part for this in 2 hours.

- Challenge 2 -> I faced here is redirection based on the REST API Action. I have used `.htaccess` to handle the different requests (GET, POST, PUT, DELETE) with only a single parameter at the end without any variable (“/people/891”). I had to parse this argument as “UUID” from HTACCESS rule.

<b> OUTCOME </b> :-

- Following ACTIONS are ready </b>:-
    - <b> GET -> http://localhost/people </b>
    - <b> GET -> http://localhost/people/891 </b>
    - <b> POST -> http://localhost/people (With JSON body) </b>
    - <b> PUT -> http://localhost/people/891  (With JSON body) </b>
    - <b> DELETE -> http://localhost/people/891 </b>

- This APIs are been developed only using Corephp and mysql. No fancy things !!!

- Enhancement needed -> Tried covering all the scenarios. But, some of the negative scenarios in the APIs are not covered as the requirement was straight forward without any sad scenarios.

### 3. Dockerize
- Database, Import process and API deployment is been automated using docker-compose.

- Just use below command to make the APIs up and running :-

    - `docker-compose build` (Assuming the docker enging is up and running)

    - `docker-compose up -d` (Assuming the docker enging is up and running)

- I have added the script in a docker-compose for db creation which will do the things needed for imports.

- This will give you the APIs on below url :-
    - <b> http://localhost (My Home Page) </b> 
    - <b> http://localhost/people </b>
    - <b> http://localhost/people/891 </b>

- Also, The easy way to see the database and schema from UI -> http://localhost:8000 (Using Phpmyadmin).

<b> Challenges </b> :- 
- It was tough to combine the db and customized script with Apache image. 
- Also, to allow my htaccess changes override from Apache using allow_override enviornment variable. It was a really nice experience though.
- I started doing all into one image using xampp / lampp and it was really huge image. I thought for the better approach and decided to go with separate image as per the need. Came up with 3 different image which will do the stuff we need as well as the light weight.

<b> All of above been developed from the scratch. </b>
