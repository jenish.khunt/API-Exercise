# Light weight based image
FROM jenishk/apache-php

# Setting up working directory
WORKDIR /app

# Copying the database import directory to the httpd root
COPY database_import /app/database_import

# Copying the csv database file for the input
COPY titanic.csv /app/database_import/